#include <iostream>
using namespace std;

int main()
{
  string one = "Code that overuses }{ GOTO statements ratherzx than_structured programminjg constructqs, resulting in convoluted and unmaintainable programs, is often called spaghetti code. Such code has a complex and tangled control structure, resulting in a program flow that is conceptually like a bowl of spaghetti, twisted and tangled.";
  cout << "People always say that my code is spaghetti, but I don't see it. Can you help me find the flag?" << endl;
  string four;
  cin >> four;
  string two =  "";

  for (int i = 0; i < 55; ++i)
  {
    two += "a";
  }

  cout << "one: " << one << endl;
  cout << "two: " << two << endl;
  cout << "four: " << four << endl;
  
  two[0]  = one[63];
  two[1]  = one[71];
  two[2]  = one[34];
  two[3]  = one[66];
  two[4]  = one[20];
  two[5]  = one[71];
  two[6]  = one[5];
  two[7]  = one[51];
  two[8]  = one[71];
  two[9]  = one[15];
  two[10] = one[51];
  two[11] = one[128];
  two[12] = one[7];
  two[13] = one[2];
  two[14] = one[51];
  two[15] = one[255];
  two[16] = one[6];
  two[17] = one[3];
  two[18] = one[34];
  two[19] = one[51];
  two[20] = one[56];
  two[21] = one[1];
  two[22] = one[2];
  two[23] = one[3];
  two[24] = one[51];
  two[25] = one[71];
  two[26] = one[15];
  two[27] = one[51];
  two[28] = one[3];
  two[29] = one[7];
  two[30] = one[15];
  two[31] = one[71];
  two[32] = one[3];
  two[33] = one[13];
  two[34] = one[51];
  two[35] = one[5];
  two[36] = one[1];
  two[37] = one[51];
  two[38] = one[13];
  two[39] = one[3];
  two[40] = one[7];
  two[41] = one[2];
  two[42] = one[51];
  two[43] = one[71];
  two[44] = one[34];
  two[45] = one[51];
  two[46] = one[7];
  two[47] = one[15];
  two[48] = one[15];
  two[49] = one[3];
  two[50] = one[32];
  two[51] = one[128];
  two[52] = one[93];
  two[53] = one[276];
  two[54] = one[19];

  cout << "one: " << one << endl;
  cout << "two: " << two << endl;
  cout << "four: " << four << endl;
  
  if (four == two)
  {
    cout << "Congratulations, you have untangled this spaghetti!" << endl;
  }
  else
  {
    cout << "Not this time!" << endl;
  }
}