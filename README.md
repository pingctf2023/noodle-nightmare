# noodle-nightmare

In the last programming session, Bajtek unleashed a coding catastrophe: his spaghetti code was so messy that even the compiler threw up its hands in surrender. Colleagues attempted to debug it, but the code was like a Rubik's Cube on a caffeine overdose. Bajtek proudly declared it an avant-garde programming masterpiece, leaving his coworkers wondering if they should call a programmer's version of an exorcist. In the end, they renamed his file "spaghetti.cpp" to "noodleNightmare.cpp" as a memorial to the chaotic session.

## solving steps

0. the code is presented as a super long series of `#includes`. In order to to read it properly i wanted to merge this includes. Using the gcc preprocessor is straightforward: `g++ -E -P noodleNightmare.cpp -o out.i`. then merging the code with a bash command: `cat our.i | tr -d ' \n' > unwrappedCode.cpp`.
0. the obtained code is a one-liner, so i formatted it with my editor and changed the name of the variables to make it more readable.
0. deleted unnecessary code
0. i added some `couts` in order to print the compared strings, and executing it i found the flag: `ping{it_is_bad_when_code_is_easier_to_read_in_assembly}`🥳.

> the final code is provided in `unwrappedCode.cpp`

## contrast with original solution

The original solution implied compiling the code and looking inside the binary. this is an approach i tried at the beginning, but due to my poor skills in decompiled navigation, i did not find the flag this way.
